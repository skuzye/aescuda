import cPickle
import numpy
from pprint import pprint

stats = {}

with open("results.bin", "rb") as fp:
    data = cPickle.load(fp)

for s, b, t in data:
    stats[s, b, t] = (numpy.mean(data[s,b,t]),
            numpy.std(data[s,b,t]))

with open("stat.bin", "wb") as fp:
    cPickle.dump(stats, fp)
