#-*- coding: utf-8 -*-
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cPickle

with open('stat.bin', 'rb') as fp:
    stat = cPickle.load(fp)

# throughput
dataGPU = [8 * s / (stat[(s, b, t)][0] * 1024) for (s, b, t) in sorted(stat.keys())
        if (b,t) == (512,512)]
dataCPU = [8 * s / (stat[(s, b, t)][0] * 1024) for (s, b, t) in sorted(stat.keys())
        if (b,t) == (1,1)]
tam = [s/1024 for (s, b, t) in sorted(stat.keys())
       if (b,t) == (512,512)]

fig = plt.figure()
axGPU = fig.add_subplot(1, 1, 1)
axGPU.plot(tam,dataGPU, marker='o', linestyle='--',
         label='GPU')
axCPU = fig.add_subplot(1, 1, 1)
axCPU.plot(tam,dataCPU, marker='o', linestyle='--',
         label='CPU')
axGPU.grid(True)
axGPU.set_xlabel('Tamanho do arquivo em MB')
axGPU.set_ylabel('Throughput (Mbps)')
axGPU.set_title('CPU vs GPU (512 blocos, 512 threads)')
plt.legend(loc='best')
# plt.annotate(u'Inversão de desempenho',
#              xy=(4, stat[(4096,512,512)][0]),
#              xytext=(4, stat[(4096,512,512)][0]+2),
#              arrowprops=dict(facecolor='black',
#                              shrink=0.05))
axGPU.set_xscale('log')
#axGPU.set_yscale('log')
axGPU.set_xticks(tam)
axGPU.get_xaxis().set_major_formatter(FormatStrFormatter('%d'))
axGPU.get_xaxis().get_major_formatter().labelOnlyBase = False
axGPU.get_yaxis().set_major_formatter(FormatStrFormatter('%.0f'))
xl = axGPU.get_xlim()
axGPU.set_xlim(1, xl[1])
plt.tight_layout()
plt.savefig('throughput.pdf')
