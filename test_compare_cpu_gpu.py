#!/usr/bin/env python2

# -*- coding: utf-8 -*-

# This script encrypts a file using PyCryto for reference and the same with
# aes-cuda and compares the two of then.

# encryption mode: CTR
# encryption key size: 256 bits

from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto import Random
import AESCuda
import array
import os
import sys
import getopt
import filecmp
import time

__TESTING__ = False
# Default gpu block, threads parameters
__GPUP__ = (128, 128)
__GPU__ = False
__CPU__ = False

def encrypt_cpu(infile, outfile, key):
    """Encrypt a file using the CPU.

    The algorithm used is AES in CTR mode.

    Keyword arguments:
    infile -- the file to be encrypted
    outfile -- the output file
    key -- key to be used for encryption, can be 128, 192 or 256 bits
    (recommended)

    """
    if not outfile:
        outfile = infile + '.enc'

    nonce = Random.new().read(AES.block_size / 2)
    ctr = Counter.new(AES.block_size * 4, prefix=nonce)
    cipher = AES.new(key, AES.MODE_CTR, counter=ctr)
    msg = array.array('B')
    chunkn = 4 * 1024 / msg.itemsize
    chunksleft = os.path.getsize(infile) / msg.itemsize

    with open(infile, 'rb') as fp:
        while chunksleft > chunkn:
            msg.fromfile(fp, chunkn)
            chunksleft = chunksleft - chunkn
        else:
            msg.fromfile(fp, chunksleft)

    if __CPU__ or not __TESTING__:
        startCPU = time.clock()
        encmsgCPU = array.array('B', nonce)
        encmsgCPU.fromstring(cipher.encrypt(msg.tostring()))
        endCPU = time.clock()
        encmsgCPU.tofile(open(outfile+'.cpu', 'wb'))

    if __GPU__ or not __TESTING__:
        startGPU = time.clock()
        encmsgGPU = array.array('B', nonce)
        encmsgGPU.fromstring(AESCuda.encrypt(msg.tostring(), key, nonce,
                                             gpup=__GPUP__))
        endGPU = time.clock()
        encmsgGPU.tofile(open(outfile+'.gpu', 'wb'))

    if not __TESTING__:
        print "CPU and GPU encrypted files are equal: "
        print filecmp.cmp(outfile+'.cpu', outfile+'.gpu')
        print("CPU encryption time: {}.".format(endCPU - startCPU))
        print("GPU encryption time: {}.".format(endGPU - startGPU))
        print("Speed-up of: {}".format((endCPU - startCPU)/(endGPU - startGPU)))
    else:
        if __CPU__:
            print(endCPU-startCPU)
        if __GPU__:
            print(endGPU-startGPU)


def decrypt_cpu(infile, outfile, key):
    """Decrypt a file using the CPU.

    The algorithm used is AES in CTR mode.

    Keyword arguments:
    infile -- the file to be encrypted
    outfile -- the output file
    key -- key to be used for encryption, can be 128, 192 or 256 bits
    (recommended)
    """

    if not outfile:
        if infile[:-4] == '.enc':
            outfile = infile[:-4]
        else:
            outfile = infile + '.dec'

    msg = array.array('B')
    chunkn = 4 * 1024 / msg.itemsize
    chunksleft = os.path.getsize(infile) / msg.itemsize

    with open(infile, 'rb') as fp:
        while chunksleft > chunkn:
            msg.fromfile(fp, chunkn)
            chunksleft = chunksleft - chunkn
        else:
            msg.fromfile(fp, chunksleft)

    # the first 8 bytes of the file is the nonce
    nonce = msg.tostring()[:8]
    ctr = Counter.new(AES.block_size * 4, prefix=nonce)
    cipher = AES.new(key, AES.MODE_CTR, counter=ctr)

    startCPU = time.clock()
    encmsgCPU = array.array('B', cipher.decrypt(msg.tostring()[8:]))
    encmsgCPU.tofile(open(outfile+'.cpu', 'wb'))
    endCPU = time.clock()

    startGPU = time.clock()
    encmsgGPU = array.array('B')
    encmsgGPU.fromstring(AESCuda.decrypt(msg.tostring()[8:], key, nonce,
                                         gpup=__GPUP__))
    encmsgGPU.tofile(open(outfile+'.gpu', 'wb'))
    endGPU = time.clock()

    if not __TESTING__:
        print "CPU and GPU decrypted files are equal :"
        print filecmp.cmp(outfile+'.cpu', outfile+'.gpu')
        print("CPU decryption time: {}.".format(endCPU - startCPU))
        print("GPU decryption time: {}.".format(endGPU - startGPU))
        print("Speed-up of: {}".format((endCPU - startCPU)/(endGPU - startGPU)))


def main(argv):
    infile = ''
    outfile = ''
    mode = ''

    try:
        opts, args = getopt.getopt(argv[1:], "gchdi:o:tp:")
    except getopt.GetoptError:
        print "usage: " + argv[0] + " -i <input> [-o <output>] [-t]"
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print "usage: " + argv[0] + " -i <input> [-o <output>] [-t]"
            sys.exit()
        elif opt == '-i':
            infile = arg
        elif opt == '-o':
            outfile = arg
        elif opt == '-d':
            mode = 'd'
        elif opt == '-t':
            global __TESTING__
            __TESTING__ = True
        elif opt == '-g':
            global __GPU__
            __GPU__ = True
        elif opt == '-c':
            global __CPU__
            __CPU__ = True
        elif opt == '-p':
            try:
                global __GPUP__
                p = map(int, arg.split(','))
                if len(p) == 2:
                    __GPUP__ = tuple(p)
                else:
                    print("Invalid -p parameters. Example: -p 128,128. " \
                          "Using defaults: 128,128.")
            except ValueError:
                print("Invalid -p parameters. Example: -p 128,128. " \
                      "Using defaults: 128,128.")

    key = b'Sixteen byte key'

    if mode == 'd':
        decrypt_cpu(infile, outfile, key)
    else:
        encrypt_cpu(infile, outfile, key)


if __name__ == "__main__":
    main(sys.argv)
