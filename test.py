#!/usr/bin/env python2

import array
import sys
import AESCuda

input = array.array("B")
key = array.array("B")
key.fromstring("Sixteen byte key")
nonce = array.array("B")
nonce.fromlist([i for i in range(8)])
input.fromlist([(i+10)%255 for i in range(16*10)])

out = AESCuda.encrypt(input, key, nonce)
outarray = array.array("B")
outarray.fromstring(out)

for i, elem in enumerate(outarray):
    if (i % 16 == 0 and i != 0):
        print ""
    sys.stdout.write(format(elem, '02x'))
print ""

