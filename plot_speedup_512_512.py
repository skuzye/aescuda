#-*- coding: utf-8 -*-
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cPickle

with open('stat.bin', 'rb') as fp:
    stat = cPickle.load(fp)

# cpu vs gpu(512, 512)
dataGPU = [stat[(s, b, t)] for (s, b, t) in sorted(stat.keys())
        if (b,t) == (512,512)]
dataCPU = [stat[(s, b, t)] for (s, b, t) in sorted(stat.keys())
        if (b,t) == (1,1)]
tam = [s/1024 for (s, b, t) in sorted(stat.keys())
       if (b,t) == (512,512)]

fig = plt.figure()
axSpeedup = fig.add_subplot(1, 1, 1)
speedup = []
for i in range(len(dataGPU)):
    speedup.append(dataCPU[i][0] / dataGPU[i][0])

axSpeedup.plot(tam, speedup,
           marker='o', linestyle='--',
           label='Speedup')
axSpeedup.grid(True)
axSpeedup.set_xlabel('Tamanho do arquivo em MB')
axSpeedup.set_ylabel(u'Proporção')
axSpeedup.set_title('Speedup entre CPU e GPU (512 blocos, 512 threads)')
plt.legend(loc='best')
axSpeedup.set_xscale('log')
#axSpeedup.set_yscale('log')
axSpeedup.set_xticks(tam)
axSpeedup.get_xaxis().set_major_formatter(FormatStrFormatter('%d'))
axSpeedup.get_xaxis().get_major_formatter().labelOnlyBase = False
axSpeedup.get_yaxis().set_major_formatter(FormatStrFormatter('%.04f'))
xl = axSpeedup.get_xlim()
axSpeedup.set_xlim(1, xl[1])
plt.savefig('speedup_512_512.pdf')
