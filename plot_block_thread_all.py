#-*- coding: utf-8 -*-
from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cPickle

# get data
with open('stat.bin', 'rb') as fp:
    stat = cPickle.load(fp)

size = [2**i for i in range(7,20)]
blocks = [2**i for i in range(6,10)]
threads = [2**i for i in range(6,10)]

x = []
y = []
time = [[] for i in size]
for i,s in enumerate(size):
    for b in blocks:
        for t in threads:
            time[i].append(stat[(s, b, t)][0])

for b in blocks:
    for t in threads:
        x.append(b)
        y.append(t)

# create plot
fig = plt.figure(1)
fig.clf()
ax = Axes3D(fig)
for i in range(len(size)):
    ax.scatter(x, y, time[i],
               marker='o', c='r')
ax.grid(True)
ax.set_xlabel('Quantidade de blocos')
ax.set_ylabel('Quantidade de threads')
ax.set_zlabel('Tempo em segundos')
ax.set_title(u'Arquivo de 32Mb')
plt.legend(loc='best')
#ax.set_xscale('log')
#ax.set_yscale('log')

ax.set_xticks(blocks)
ax.set_yticks(threads)
ax.get_xaxis().set_major_formatter(FormatStrFormatter('%d'))
ax.get_xaxis().get_major_formatter().labelOnlyBase = False
ax.get_yaxis().set_major_formatter(FormatStrFormatter('%d'))
ax.get_yaxis().get_major_formatter().labelOnlyBase = False

#xl = ax.get_xlim()
#ax.set_xlim(1, xl[1])
plt.savefig('block_thread_all_sizes.pdf')
