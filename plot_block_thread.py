#-*- coding: utf-8 -*-
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import cPickle

ind = np.arange(4)
width = 0.15

# get data
with open('stat.bin', 'rb') as fp:
    stat = cPickle.load(fp)

blocks = [2**i for i in range(6,10)]
threads = [2**i for i in range(6,10)]
s = 32 * 1024

time = []
sd = []
for t in threads:
    time.append([stat[(s, b, t)][0] for b in blocks])
    sd.append([stat[(s, b, t)][1] for b in blocks])

# create plot
fig = plt.figure()
ax = fig.add_subplot(111)

threads64 = ax.bar(ind, time[0], width, color='r', yerr=sd[0], align='center')
threads128 = ax.bar(ind+width, time[1], width, color='g', yerr=sd[1], align='center')
threads256 = ax.bar(ind+width*2, time[2], width, color='b', yerr=sd[2], align='center')
threads512 = ax.bar(ind+width*3, time[3], width, color='y', yerr=sd[3], align='center')

ax.autoscale(tight=True)

ax.set_ylabel('Tempo (s)')
ax.set_xticks(ind+width*1.5)
ax.set_xticklabels( [str(i)+" blocos" for i in blocks] )
ax.legend( [str(i)+" threads" for i in threads] )
ax.set_title(u'Várias configurações block/thread para arquivo de 32Mb')
x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,0.25))

plt.savefig('block_thread.pdf')
