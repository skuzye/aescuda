CUDA_INSTALL_PATH ?= /opt/cuda
 
CXX := g++ 
CC := gcc
LINK := g++ -fPIC
NVCC := nvcc
 
# Includes
INCLUDES = -I. -I$(CUDA_INSTALL_PATH)/include
COMMONFLAGS += $(INCLUDES) -g
NVCCFLAGS += $(COMMONFLAGS) -arch=sm_21 -shared -Xcompiler -fPIC
CXXFLAGS += $(COMMONFLAGS) -Wall -shared -fPIC
CFLAGS += $(COMMONFLAGS) -Wall -std=c99 -shared -fPIC
 
LIB_CUDA := -L$(CUDA_INSTALL_PATH)/lib64 -lcudart -lcuda
 
.SUFFIXES: .c .cpp .cu .o .h
 
OBJS = src/test.c.o src/cuda/aes.cu.o \
	src/const.c.o src/key_expansion.c.o

OBJS_LIB = src/AESCuda.o src/cuda/aes.cu.o \
	   src/const.c.o src/key_expansion.c.o

SOURCE_LIB = src/AESCuda.c
 
%.c.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@
 
%.cu.o: %.cu
	$(NVCC) $(NVCCFLAGS) -c $< -o $@
 
%.cpp.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@
 
all: build lib
 
build: aes
 
aes: $(OBJS)
	$(LINK) -o $@ $^ $(LIB_CUDA)

src/AESCuda.o: src/AESCuda.c
	gcc $(CFLAGS) -o src/AESCuda.o -shared -c src/AESCuda.c -fPIC -I/usr/include/python2.7

lib: $(OBJS_LIB)
	gcc -o AESCuda.so -shared $^ -fPIC $(LIB_CUDA)

clean:
	rm -rf aes src/*.o src/cuda/*.o AESCuda.so
