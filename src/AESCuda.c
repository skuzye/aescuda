/*
* aescuda - encrypt using AES and CUDA
* Copyright (C) 2012  "Leandro M. Barbosa" <lbarbosa@linux.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/



/* make functions return Py_ssize_t */
#define PY_SSIZE_T_CLEAN
#define NDEBUG

#include <Python.h>
#include "include/const.h"
#include "cuda/aes.h"

static PyObject *
AESCuda_encrypt(PyObject *self, PyObject *args, PyObject *keywds)
{
    unsigned char* input;
    Py_ssize_t input_size;
    unsigned char* key;
    Py_ssize_t key_size;
    unsigned char* nonce;
    Py_ssize_t nonce_size;
    unsigned long ctr_offset = 0;
    GpuAes_Params gpup;
    unsigned char* output;
    PyObject * result;
    PyObject* PyGpup = NULL;
    PyObject* PyGpup_seq;
    int PyGpup_size;

    GpuAes_Params_Init(&gpup);

    static char *kwlist[] = {"input", "key", "nonce", "ctr_offset", "gpup",
                             NULL};

    /* TODO: check if all arguments are valid */

    if (!PyArg_ParseTupleAndKeywords(args, keywds,
                                     "s#s#s#|kO",
                                     kwlist,
                                     &input, &input_size,
                                     &key, &key_size,
                                     &nonce, &nonce_size,
                                     &ctr_offset,
                                     &PyGpup)) {
        return NULL;
    }

    /* Debug printing */
#ifndef NDEBUG
    printf("input size: %zd\n", input_size);
    puts("input:");
    for (Py_ssize_t k = 0; k < input_size; k++) {
        if (k % 16 == 0 && k != 0) {
            puts("");
        }
        printf("%02x", input[k]);
    }
    puts("");

    printf("key size: %zd\n", key_size);
    puts("key:");
    for (Py_ssize_t k = 0; k < key_size; k++) {
        if (k % 16 == 0 && k != 0) {
            puts("");
        }
        printf("%02x", key[k]);
    }
    puts("");

    printf("nonce size: %zd\n", nonce_size);
    puts("nonce:");
    for (Py_ssize_t k = 0; k < nonce_size; k++) {
        if (k % 16 == 0 && k != 0) {
            puts("");
        }
        printf("%02x", nonce[k]);
    }
    puts("");

    printf("offset: %lu\n", ctr_offset);

#endif
   if (PyGpup != NULL) {
        PyGpup_seq = PySequence_Fast(PyGpup, "expected a sequence");
        PyGpup_size = PySequence_Size(PyGpup_seq);
        if (PyGpup_size == 2) {
            gpup.blocks = (unsigned int) PyInt_AsUnsignedLongMask(
                PySequence_Fast_GET_ITEM(PyGpup_seq, 0));
            gpup.threads = (unsigned int) PyInt_AsUnsignedLongMask(
                PySequence_Fast_GET_ITEM(PyGpup_seq, 1));
        }
    }

    output = GpuAes_Encrypt(input,
                         input_size,
                         key,
                         nonce,
                         ctr_offset,
                         &gpup);

    if (output == NULL) {
        Py_RETURN_NONE;
    }

    result = Py_BuildValue("s#", output, input_size);
    GpuAes_Free(output);


    return result;
}

static PyMethodDef AESCudaMethods[] = {
    {"encrypt", (PyCFunction)AESCuda_encrypt, METH_VARARGS | METH_KEYWORDS,
     "Encrypt a message using AES on the GPU."},
    {"decrypt", (PyCFunction)AESCuda_encrypt, METH_VARARGS | METH_KEYWORDS,
     "Decrypt a message using AES on the CPU."},
    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
initAESCuda(void)
{
    (void) Py_InitModule("AESCuda", AESCudaMethods);
}
