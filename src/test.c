/*
* aescuda - encrypt using AES and CUDA
* Copyright (C) 2012  "Leandro M. Barbosa" <lbarbosa@linux.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>

/* gpu aes */
#include "cuda/aes.h"
#include "include/const.h"

#define BUFFER_SIZE 4096

/* key length in bits */
#define KEY_LENGTH 128

int main(int argc, char** argv)
{
    unsigned char* input;
    unsigned long int input_size;
    unsigned char* output;
    unsigned char* key = (unsigned char*) malloc(KEY_LENGTH);
    GpuAes_Params p;
    unsigned char* buffer = (unsigned char*) malloc(BUFFER_SIZE);
    unsigned char nonce[8];
    /* buffer to read the key */
    char buffer_key[33];
    unsigned int temp;
    struct stat st;

    FILE *fp;

    fp = fopen("key", "r");
    /* read key from standard input */
    /* only 128 bits is supported yet */
    fgets(buffer_key, 33, fp);
    fclose(fp);

    /* convert string in buffer to hex and store in a matrix in a row major
     * order */
    {
        int pos = 0;
        for (int i = 0; i < BLOCK_LENGTH / 4; i++) {
            for (int j = 0; j < BLOCK_LENGTH / 4; j++) {
                sscanf(&buffer_key[pos], "%2x", &temp);
                key[NB * i + j] = (char) temp;
                pos += 2;
            }
        }
    }

    stat("teste", &st);
    input_size = (unsigned long int) st.st_size;
    input = (unsigned char *) malloc(input_size * sizeof(unsigned char));
    fp = fopen("teste", "rb");
    fread(input, sizeof(unsigned char), input_size, fp);
    fclose(fp);

    /* initialize the nonce */
    for (uint_fast8_t i = 0; i < 8; ++i) {
        nonce[i] = 0;
    }
    /* Load default params */
    GpuAes_Params_Init(&p);

    /* encrypt using GPU */
    if ((output = GpuAes_Encrypt(input,
                       input_size,
                       key,
                       nonce,
                       0,
                       &p)) == NULL) {
        fprintf(stderr, "\nThere were errors encrypting with GPU\n");
        free(input);
        free(buffer);
        GpuAes_Free(output);
        free(key);
        return 1;
    }

//    fp = fopen("teste.enc.gpu.c", "wb");
//    fwrite(nonce, sizeof(unsigned char), 8, fp);
//    fwrite(output, sizeof(unsigned char), input_size, fp);
//    fclose(fp);

    free(input);
    GpuAes_Free(output);
    free(buffer);
    free(key);

    return 0;
}
