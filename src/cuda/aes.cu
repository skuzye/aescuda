/*
* aescuda - encrypt using AES and CUDA
* Copyright (C) 2012  "Leandro M. Barbosa" <lbarbosa@linux.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/



#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdint.h>

extern "C" {
#include "aes.h"
#include "../include/key_expansion.h"
#include "../include/const.h"
}

__constant__ uint_fast32_t c_ekey[NB * 11 * sizeof(uint_fast32_t)];
__constant__ unsigned char c_nonce[8];
extern "C" __device__ __forceinline__ static void
AddRoundKeyGPU(unsigned char* state,
               uint_fast8_t round)
{
	#pragma unroll
    for (uint_fast8_t j = 0; j < NB; ++j) {
        state[0 * NB + j] ^= (unsigned char) (c_ekey[NB * round + j] >>
                                     24);
        state[1 * NB + j] ^= (unsigned char) (c_ekey[NB * round + j] >>
                                     16);
        state[2 * NB + j] ^= (unsigned char) (c_ekey[NB * round + j] >>
                                     8);
        state[3 * NB + j] ^= (unsigned char) (c_ekey[NB * round + j] &
                                     0xFF);
    }
}

extern "C" __device__ __forceinline__ static void
SubBytesGPU(unsigned char* state,
            unsigned char* sbox)
{
    /* location of the byte to be grabbed in the Sbox */
    uint_fast8_t m, n;
	#pragma unroll
    for (uint_fast8_t i = 0; i < 4; i++)
		#pragma unroll
        for (uint_fast8_t j = 0; j < NB; j++) {
            m = (state[i * NB + j] >> 4) & 0x0F;
            n = state[i * NB + j] & 0x0F;

            state[i * NB + j] = sbox[16 * m + n];
        }
}

extern "C" __device__ __forceinline__ static void
ShiftRowsGPU(unsigned char* state)
{
    unsigned char temp;
    /* for each row */

    #pragma unroll
    for (uint_fast8_t i = 1; i < 4; ++i) {
		#pragma unroll
        for (uint_fast8_t j = 0; j < i; ++j) {
            temp = state[i * NB];
			#pragma unroll
            for (uint_fast8_t k = 0; k < NB - 1; ++k) {
                state[i * NB + k] = state[i * NB + k + 1];
            }
            state[i * NB + NB - 1] = temp;
        }
    }
}

extern "C" __device__ __forceinline__ static void
MixColumnsGPU(unsigned char* state)
{
    unsigned char a[4];
    unsigned char b[4];
    unsigned char h;

	#pragma unroll
    for(uint_fast8_t i = 0; i < NB; ++i) {
	#pragma unroll
        for (uint_fast8_t j = 0; j < 4; ++j) {
            a[j] = state[j * NB + i];
            h =  (unsigned char)((signed char) state[j * NB + i] >> 7);
            b[j] = state[j * NB + i] << 1;
            b[j] ^= 0x1B & h;
        }
        state[0 * NB + i] = b[0] ^ a[3] ^ a[2] ^ b[1] ^ a[1];
        state[1 * NB + i] = b[1] ^ a[0] ^ a[3] ^ b[2] ^ a[2];
        state[2 * NB + i] = b[2] ^ a[1] ^ a[0] ^ b[3] ^ a[3];
        state[3 * NB + i] = b[3] ^ a[2] ^ a[1] ^ b[0] ^ a[0];
    }
}

extern "C" __global__ void
aes_ctrGPU(unsigned char* ctr_blocks,
           uint_fast32_t n_ctr_blocks,
           unsigned char* sbox,
           uint_fast32_t ctr_offset)
{
    /* unsigned int tid = threadIdx.x; */
    uint_fast32_t tid = threadIdx.x + blockIdx.x * blockDim.x;

    /* allocate block memory */
    unsigned char state[BLOCK_LENGTH];

    while (tid < n_ctr_blocks) {
        /* copy nonce from global memory
           It has to be stored in a columns because of how
           AES expects it to be */
        {
            uint_fast8_t k = 0;
			#pragma unroll
            for (uint_fast8_t i = 0; i < 2; ++i) {
				#pragma unroll
                for (uint_fast8_t j = 0; j < 4; ++j) {
                    state[NB * j + i] = c_nonce[k];
                    k++;
                }
            }
        }

        {
            uint_fast8_t k = 0;
            for (uint_fast8_t i = 0; i < 4; ++i) {
                for (uint_fast8_t j = 0; j < 4; ++j) {
                    state[NB * j + i] = ctr_blocks[k];
                    k++;
                }
            }
        }

        /* generate the rest of the block based on tid */
        /* this is a base 256 conversion of tid which has 32bits in size. */
        /* As we have 64 bits available we will fill the first 32 bits with 0s.
           Note: we are storing this number in a big endian fashion */
		#pragma unroll
        for (uint_fast8_t i = 0; i < 4; ++i) {
            state[NB * i + 2] = 0;
        }
        {
            int_fast8_t i;
            uint_fast8_t k;
			#pragma unroll
            for (i = 3, k = 0; i >= 0; --i, k += 8) {
                state[NB * i + 3] =
                    (unsigned char) ((tid+1) >> k) & 0xff;
            }
        }

        AddRoundKeyGPU(state, 0);
		#pragma unroll
        for (int round = 1; round <= 9; ++round) {
            SubBytesGPU(state, sbox);
            ShiftRowsGPU(state);
            MixColumnsGPU(state);
            AddRoundKeyGPU(state, round);
        }

        SubBytesGPU(state, sbox);
        ShiftRowsGPU(state);
        AddRoundKeyGPU(state, 10);

        /* Copy the cache back to global memory */
		#pragma unroll
        for (uint_fast8_t i = 0; i < 4; ++i) {
			#pragma unroll
            for (uint_fast8_t j = 0; j < NB; ++j) {
                ctr_blocks[BLOCK_LENGTH * tid + NB * i + j] ^=
                    state[NB * j + i];
            }
        }

        tid += blockDim.x * gridDim.x;
    }
}

extern "C"
unsigned char*
GpuAes_Encrypt(unsigned char* input,
               unsigned long int input_size,
               unsigned char* key,
               unsigned char* nonce,
               unsigned int ctr_offset,
               GpuAes_Params* p)
{
    size_t key_size = NB * 11 * sizeof(uint_fast32_t);
    uint_fast32_t* ekey = (uint_fast32_t *) malloc(key_size);
    unsigned char* d_ctr_blocks;
    unsigned char* d_sbox;
    cudaError_t ret;
    unsigned long int nblocks;
    unsigned char* output;

    /* expand the key on cpu */
    KeyExpansion(key, ekey, 4);

    nblocks = input_size / BLOCK_LENGTH;
    if (input_size % BLOCK_LENGTH != 0) {
        nblocks++;
    }

    /* GPU processing */
    /* allocate host pinned memory */
    cudaHostAlloc((void **) &output, input_size, cudaHostAllocDefault);
    /* allocate blocks memory */
    ret = cudaMalloc((void **) &d_ctr_blocks,
                     nblocks * BLOCK_LENGTH * sizeof(unsigned char));
    if (ret != cudaSuccess) {
        fprintf(stderr, cudaGetErrorString(ret));
        goto error;
    }
    ret = cudaMalloc((void **) &d_sbox, 256 * sizeof(unsigned char));
    if (ret != cudaSuccess) {
        fprintf(stderr, cudaGetErrorString(ret));
        goto error;
    }
    /* copy key to device memory */
    ret = cudaMemcpyToSymbol(c_ekey, ekey, key_size);
    if (ret != cudaSuccess) {
        fprintf(stderr, cudaGetErrorString(ret));
        goto error;
    }
    /* copy plaintext to device */
    ret = cudaMemcpy(d_ctr_blocks,
                     input,
                     input_size * sizeof (unsigned char),
                     cudaMemcpyHostToDevice);
    if (ret != cudaSuccess) {
        fprintf(stderr, cudaGetErrorString(ret));
        goto error;
    }
    /* copy sbox to device memory */
    ret = cudaMemcpy(d_sbox, Sbox,
                     256 * sizeof(unsigned char),
                     cudaMemcpyHostToDevice);
    if (ret != cudaSuccess) {
        fprintf(stderr, cudaGetErrorString(ret));
        goto error;
    }
    /* copy nonce to device memory */
    ret = cudaMemcpyToSymbol(c_nonce, nonce,
                     8 * sizeof(unsigned char));
    if (ret != cudaSuccess) {
        fprintf(stderr, cudaGetErrorString(ret));
        goto error;
    }

    /* launch kernel in GPU */
    aes_ctrGPU<<<p->blocks, p->threads>>>(d_ctr_blocks,
                        nblocks,
                        d_sbox,
                        ctr_offset);
    ret = cudaGetLastError();
    if (ret != cudaSuccess) {
        fprintf(stderr, cudaGetErrorString(ret));
        goto error;
    }
    /* copy result from device */
    ret = cudaMemcpy(output,
                     d_ctr_blocks,
                     input_size * sizeof (unsigned char),
                     cudaMemcpyDeviceToHost);
    if (ret != cudaSuccess) {
        fprintf(stderr, cudaGetErrorString(ret));
        goto error;
    }

    /* free device memory */
    cudaFree(d_ctr_blocks);
    cudaFree(d_sbox);

    return output;

error:
    cudaFree(d_ctr_blocks);
    cudaFree(d_sbox);

    return NULL;
}

extern "C"
unsigned char*
GpuAes_Decrypt(unsigned char* input,
               unsigned long int input_size,
               unsigned char* key,
               unsigned char* nonce,
               unsigned int ctr_offset,
               GpuAes_Params* p)
{
    return GpuAes_Encrypt(input,
                          input_size,
                          key,
                          nonce,
                          ctr_offset,
                          p);
}

void GpuAes_Params_Init(GpuAes_Params* p)
{
    p->blocks = 128;
    p->threads = 128;
}

void GpuAes_Free(void * p)
{
	cudaFreeHost(p);
}
