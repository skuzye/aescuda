/*
* aescuda - encrypt using AES and CUDA
* Copyright (C) 2012  "Leandro M. Barbosa" <lbarbosa@linux.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/



#ifndef AES_CU_H
#define AES_CU_H

typedef struct GpuAes_Params GpuAes_Params;

struct GpuAes_Params {
    unsigned int blocks;
    unsigned int threads;
};

/* nonce size should be 8 bytes */
unsigned char*
GpuAes_Encrypt(unsigned char* input,
               unsigned long int input_size,
               unsigned char* key,
               unsigned char* nonce,
               unsigned int ctr_offset,
               GpuAes_Params* params);

unsigned char*
GpuAes_Decrypt(unsigned char* input,
               unsigned long int input_size,
               unsigned char* key,
               unsigned char* nonce,
               unsigned int ctr_offset,
               GpuAes_Params* p);

void GpuAes_Params_Init(GpuAes_Params* params);

void GpuAes_Free(void * p);

#endif
