/*
* aescuda - encrypt using AES and CUDA
* Copyright (C) 2012  "Leandro M. Barbosa" <lbarbosa@linux.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/



#ifndef CONST_H_
#define CONST_H_

/* Tables */
/* block length in bytes */
#define BLOCK_LENGTH 16
#define NB (BLOCK_LENGTH / 4)

#define BUFFER_SIZE 4096

/* key length in bits */
#define KEY_LENGTH 128

extern unsigned char Sbox[256];

/* inverse S-box */
extern unsigned char InvSbox[256]; 

/* lookup table to multiply by 9 */
extern unsigned char mul9[256];

/* lookup table to multiply by 11 */
extern unsigned char mul11[256];

/* lookup table to multiply by 13 */
extern unsigned char mul13[256];

/* lookup table to multiply by 14 */
extern unsigned char mul14[256];

#endif

