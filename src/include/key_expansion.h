/*
* aescuda - encrypt using AES and CUDA
* Copyright (C) 2012  "Leandro M. Barbosa" <lbarbosa@linux.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/



#ifndef KEY_EXPANSION_H_
#define KEY_EXPANSION_H_

#include "const.h"
#include <stdint.h>

/* Substitue each byte of the word using the SBox */
uint_fast32_t SubWord(uint_fast32_t w);

/* Rotate a word that is, take a word with byte ABCD and return BCDA.
 * In other words, does a cyclic left shift */
uint_fast32_t RotWord(uint_fast32_t w);

/* Expand key into ekey.
 * Nb = number of columns in a block (4 for AES).
 * Nr = number of rounds to be performed: 10, 12, 14 for key sizes 128 bits,
 * 192 bits, 256 bits, respectively.
 * Nk = Number of words in the key. (4, 6 or 8).
 */
void KeyExpansion(const unsigned char* key, uint_fast32_t* ekey, uint_fast8_t Nk);

#endif
