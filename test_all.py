import subprocess
import cPickle
from pprint import pprint

filesizes = [2**i for i in range(7,20)]
print("filesizes: {}".format(filesizes))
blocks = [2**i for i in range(6,10)]
print("blocks: {}".format(blocks))
threads = [2**i for i in range(6,10)]
print("threads: {}".format(threads))
times = 10
print("times: {}".format(times))

# create files
for size in filesizes:
    subprocess.call('dd if=/dev/urandom of=test{} bs=1024 count={}'.format(size, size), shell=True)

results = {}
for s in filesizes:
    # GPU
    for b in blocks:
        for t in threads:
            results[(s,b,t)] = []
            for i in range(times):
                try:
                    r = subprocess.check_output(['./test_compare_cpu_gpu.py',
                        '-i',
                        'test{}'.format(s),
                        '-t',
                        '-g'])
                    results[(s,b,t)].append(r.split('\n')[0])
                except:
                    print("Failed at ({},{},{})".format(s,b, ))

    # CPU
    results[(s,1,1)] = []
    for i in range(times):
        r = subprocess.check_output(['./test_compare_cpu_gpu.py',
                                     '-i',
                                     'test{}'.format(s),
                                     '-t',
                                     '-c'])
        results[(s,1,1)].append(float(r.split('\n')[0]))

with open("results.bin", "wb") as fp:
    cPickle.dump(results, fp)

pprint(results)
